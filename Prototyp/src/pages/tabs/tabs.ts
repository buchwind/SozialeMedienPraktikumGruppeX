import { Component } from '@angular/core';

import { LocationPage } from '../location/location';
import { VicinityPage } from '../vicinity/vicinity';
import { LunchPage } from '../lunch/lunch';
import { SettingsPage } from '../settings/settings';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = LocationPage;
  tab3Root = VicinityPage;
  tab4Root = LunchPage;
  tab5Root = SettingsPage;

  constructor() {

  }
}
