import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-lunch',
  templateUrl: 'lunch.html'
})
export class LunchPage {

  constructor(public navCtrl: NavController) {

  }

  logout() {
    // TODO Auth Service
    this.navCtrl.push(LoginPage);
  }

}
