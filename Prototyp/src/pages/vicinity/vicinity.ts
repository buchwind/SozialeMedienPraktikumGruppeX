import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-vicinity',
  templateUrl: 'vicinity.html'
})
export class VicinityPage {

  constructor(public navCtrl: NavController) {

  }

  logout() {
    // TODO Auth Service
    this.navCtrl.push(LoginPage);
  }

}
