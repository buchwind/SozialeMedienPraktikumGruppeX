import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-location',
  templateUrl: 'location.html'
})
export class LocationPage {

  constructor(public navCtrl: NavController) {
  }

  logout() {
    // TODO Auth Service
    this.navCtrl.push(LoginPage);
  }

  mailTo(email) {
    let Link=`mailto:${email}`;
    window.open(Link, "_system");
  }
}
