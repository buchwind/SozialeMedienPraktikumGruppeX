GruppeX - Soziale Medien Praktikum

Mitglieder:
<ul>
<li>Dennis Buchwinkler (buchwind)</li>
<li>Lars Kolmetz (kolmetzl)</li>
<li>Eugen Puzynin (puzynine)</li>
<li>Sophia Mühling (muelings)</li>
<li>Dennis Barsuhn (barsuhnd)</li>
<li>André Niendorf (niendora)</li>
</ul>

Aufgabe: Konzeptionierung / Implementierung einer Standort-App Mitarbeiter von für Campana & Schott

bisherige User Stories:
<ul>
<li>Standortermittlung: Mitarbeiter wählen einen Standort über GPS automatisch oder manuell;</li>
<li><ul>Standortinformationen: Alle wichtigen Infos zu dem Standort werden angezeigt:
<li>Adresse, Telefon, Partnerhotels in der Umgebung, Tipps,</li>
<li>Restaurants/Essen in der Nähe, Stammlokale (mit Empfehlungen/Bewertungen von Mitarbeitern),</li>
<li>Anzeigen von Profilen der Mitarbeiter am Standort,</li>
<li>lokale Events vom Office;</li>
</ul></li>
<li>„Let’s get Lunch“ Funktion: wöchentliche Zuweisung eines Lunchpartners nach Zufallsprinzip, 
um alle Mitarbeiter an dem Standort besser kennenzulernen und sich auszutauschen.</li>
</ul>